package de.mightyfirebird.carma.security;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class JwtTokenProvider {
	/**
	 * The Secret for signing the token.
	 */
	@Value("${app.jwtSecret}")
	private String jwtSecret;

	/**
	 * The expiration-time of the token in milliseconds.
	 */
	@Value("${app.jwtExpirationInMs}")
	private int jwtExpirationInMs;

	/**
	 * Generates a JSON Web Token.
	 *
	 * @param authentication Spring Security authentication-Object
	 * @return the JSON Web Token
	 */
	public String generateToken(final Authentication authentication) {
		final UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

		final Date now = new Date();
		final Date expirationDate = new Date(now.getTime() + jwtExpirationInMs);

		return Jwts.builder()
				.setSubject(Long.toString(userPrincipal.getId()))
				.setIssuedAt(new Date())
				.setExpiration(expirationDate)
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}

	/**
	 * Retrieves the user-ID from the JSON Web Token.
	 *
	 * @param token the JSON Web Token
	 * @return the user-ID
	 */
	public Long getUserIdFromToken(final String token) {
		final Claims claims =
				Jwts.parser()
						.setSigningKey(jwtSecret)
						.parseClaimsJws(token)
						.getBody();
		return NumberUtils.toLong(claims.getSubject());
	}

	/**
	 * Checks, if the token is valid.
	 *
	 * @param token the JSON Web Token
	 * @return true, if the token is valid, false otherwise
	 */
	public boolean isTokenValid(final String token) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
			return true;
		} catch (final SignatureException ex) {
			log.error("Invalid JWT signature");
		} catch (final MalformedJwtException ex) {
			log.error("Invalid JWT token");
		} catch (final ExpiredJwtException ex) {
			log.error("Expired JWT token");
		} catch (final UnsupportedJwtException ex) {
			log.error("Unsupported JWT token");
		} catch (final IllegalArgumentException ex) {
			log.error("JWT claims string is empty.");
		}
		return false;
	}
}
