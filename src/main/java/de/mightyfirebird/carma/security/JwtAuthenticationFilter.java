package de.mightyfirebird.carma.security;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private CustomUserDetailService customUserDetailService;

	@Override
	protected void doFilterInternal(final HttpServletRequest httpServletRequest,
	                                final HttpServletResponse httpServletResponse,
	                                final FilterChain filterChain) throws ServletException, IOException {
		try {
			final String token = getJwtFromRequest(httpServletRequest);
			if (StringUtils.isNotBlank(token) && jwtTokenProvider.isTokenValid(token)) {
				final Long userId = jwtTokenProvider.getUserIdFromToken(token);
				final UserDetails userDetails = customUserDetailService.loadUserById(userId);
				final UsernamePasswordAuthenticationToken authenticationToken =
						new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
				SecurityContextHolder.getContext().setAuthentication(authenticationToken);
			}
		} catch (final Exception e) {
			log.error("Could not set user authentication in security context", e);
		}

		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}

	private String getJwtFromRequest(final HttpServletRequest httpServletRequest) {
		final String bearerToken = httpServletRequest.getHeader("Authorization");
		if (StringUtils.isNotBlank(bearerToken) && StringUtils.startsWith(bearerToken, "Bearer ")) {
			return StringUtils.substring(bearerToken, 7);
		}
		return null;
	}
}
