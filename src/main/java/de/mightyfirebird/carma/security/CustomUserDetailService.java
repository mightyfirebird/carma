package de.mightyfirebird.carma.security;

import de.mightyfirebird.carma.model.User;
import de.mightyfirebird.carma.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailService implements UserDetailsService {
	/**
	 * The {@link UserRepository}.
	 */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Load user by username or email.
	 * <p>
	 * This method is used by Spring Security.
	 * </p>
	 *
	 * @param usernameOrEmail Username or E-Mail-Address
	 * @return found User
	 * @throws UsernameNotFoundException if no user could be found
	 */
	@Override
	@Transactional
	public UserDetails loadUserByUsername(final String usernameOrEmail) throws UsernameNotFoundException {
		final User user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
				.orElseThrow(
						() -> new UsernameNotFoundException(String.format("No user found with username or e-mail-address '%s'", usernameOrEmail)));
		return UserPrincipal.create(user);
	}

	/**
	 * Load user by ID.
	 * <p>
	 * This method ist used by {@link JwtAuthenticationFilter}.
	 * </p>
	 *
	 * @param id User-ID
	 * @return found User
	 * @throws UsernameNotFoundException if no user could be found
	 */
	@Transactional
	public UserDetails loadUserById(final Long id) throws UsernameNotFoundException {
		final User user = userRepository.findById(id).orElseThrow(
				() -> new UsernameNotFoundException("User not found with id : " + id)
		);
		return UserPrincipal.create(user);
	}
}
