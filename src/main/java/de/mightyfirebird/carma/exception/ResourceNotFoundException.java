package de.mightyfirebird.carma.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
	private String resourceName;
	private String fieldName;
	private Object fieldValue;

	/**
	 * Constructor.
	 *
	 * @param resourceName the name of the resource
	 * @param fieldName    the name of the field
	 * @param fieldValue   the value of the field
	 */
	public ResourceNotFoundException(final String resourceName, final String fieldName, final Object fieldValue) {
		super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue));
		this.resourceName = resourceName;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
	}
}

