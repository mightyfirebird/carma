package de.mightyfirebird.carma.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
public class UserProfile {
    private Long id;
    private String firstname;
    private String lastname;
    private String username;
    private Instant joinedAt;
}
