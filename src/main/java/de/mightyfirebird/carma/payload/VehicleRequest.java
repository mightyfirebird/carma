package de.mightyfirebird.carma.payload;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class VehicleRequest {
    @NotBlank
    @Size(max = 50)
    private String make;

    @NotBlank
    @Size(max = 50)
    private String model;

    @NotBlank
    @Size(max = 50)
    private String subModel;

    @NotNull
    @Max(value = 9999)
    private int year;

    @NotBlank
    @Size(max = 50)
    private String engine;

    @NotBlank
    @Size(max = 500)
    private String note;
}
