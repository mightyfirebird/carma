package de.mightyfirebird.carma.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtAuthenticationResponse {
	private String accessToken;
	private String tokenType = "Bearer";

	/**
	 * Constructor.
	 *
	 * @param accessToken JSON Web Token
	 */
	public JwtAuthenticationResponse(final String accessToken) {
		this.accessToken = accessToken;
	}
}
