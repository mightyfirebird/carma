package de.mightyfirebird.carma.payload;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class SignUpRequest {
	@NotBlank
	@Size(min = 4, max = 50)
	private String firstname;

	@NotBlank
	@Size(min = 4, max = 50)
	private String lastname;

	@NotBlank
	@Size(min = 4, max = 50)
	private String username;

	@NotBlank
	@Size(max = 255)
	@Email
	private String email;

	@NotBlank
	@Size(min = 8, max = 100)
	private String password;

}
