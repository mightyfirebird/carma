package de.mightyfirebird.carma.payload;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class VehicleResponse {
	private String make;
	private String model;
	private String subModel;
	private int year;
	private String engine;
	private String note;

	private UserSummary createdBy;
	private Instant creationDateTime;
}
