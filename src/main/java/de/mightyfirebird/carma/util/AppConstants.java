package de.mightyfirebird.carma.util;

/**
 * Application Constants.
 *
 * @author MightyFirebird
 * @version 7/10/2018
 */
public class AppConstants {
	public static final String DEFAULT_PAGE_NUMBER = "0";
	public static final String DEFAULT_PAGE_SIZE = "30";

	public static final int MAX_PAGE_SIZE = 50;

	/**
	 * Private Constructor.
	 */
	private AppConstants() {
	}
}
