package de.mightyfirebird.carma.controller;

import de.mightyfirebird.carma.exception.AppException;
import de.mightyfirebird.carma.model.Role;
import de.mightyfirebird.carma.model.RoleName;
import de.mightyfirebird.carma.model.User;
import de.mightyfirebird.carma.payload.ApiResponse;
import de.mightyfirebird.carma.payload.JwtAuthenticationResponse;
import de.mightyfirebird.carma.payload.LoginRequest;
import de.mightyfirebird.carma.payload.SignUpRequest;
import de.mightyfirebird.carma.repository.RoleRepository;
import de.mightyfirebird.carma.repository.UserRepository;
import de.mightyfirebird.carma.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		final Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						loginRequest.getUsernameOrEmail(),
						loginRequest.getPassword()
				)
		);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		final String token = tokenProvider.generateToken(authentication);
		return ResponseEntity.ok(new JwtAuthenticationResponse(token));
	}

	@PostMapping("/register")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest request) {
		if (userRepository.existsByUsername(request.getUsername())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}
		if (userRepository.existsByEmail(request.getEmail())) {
			return new ResponseEntity(new ApiResponse(false, "Email address already in use!"), HttpStatus.BAD_REQUEST);
		}

		final User user = new User(request.getFirstname(), request.getLastname(), request.getUsername(), request.getEmail(), request.getPassword());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		final Role role = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new AppException("User role not set!"));
		user.setRoles(Collections.singleton(role));

		final User savedUser = userRepository.save(user);

		final URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(savedUser.getUsername()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully!"));
	}
}
