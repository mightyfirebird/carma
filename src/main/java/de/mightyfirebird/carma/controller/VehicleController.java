package de.mightyfirebird.carma.controller;

import de.mightyfirebird.carma.exception.ResourceNotFoundException;
import de.mightyfirebird.carma.model.User;
import de.mightyfirebird.carma.model.Vehicle;
import de.mightyfirebird.carma.payload.ApiResponse;
import de.mightyfirebird.carma.payload.VehicleRequest;
import de.mightyfirebird.carma.repository.VehicleRepository;
import de.mightyfirebird.carma.security.CurrentUser;
import de.mightyfirebird.carma.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

	@Autowired
	private VehicleRepository vehicleRepository;

	@GetMapping
	public List<Vehicle> getVehicles(@CurrentUser UserPrincipal currentUser) {
		return vehicleRepository.findByUser(currentUser.getId());
	}

	@PostMapping
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity createVehicle(@CurrentUser UserPrincipal currentUser,
	                                    @Valid @RequestBody VehicleRequest vehicleRequest) {
		final Vehicle vehicle = new Vehicle();
		vehicle.setMake(vehicleRequest.getMake());
		vehicle.setModel(vehicleRequest.getModel());
		vehicle.setSubModel(vehicleRequest.getSubModel());
		vehicle.setYear(vehicleRequest.getYear());
		vehicle.setEngine(vehicleRequest.getEngine());
		vehicle.setNote(vehicleRequest.getNote());
		final User user = new User();
		user.setId(currentUser.getId());
		vehicle.setUser(user);
		Vehicle savedVehicle = vehicleRepository.save(vehicle);

		final URI location = ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{pollId}")
				.buildAndExpand(savedVehicle.getId()).toUri();
		return ResponseEntity.created(location)
				.body(new ApiResponse(true, "Vehicle successfully created!"));
	}

	@GetMapping("/{vehicleId}")
	public Vehicle getVehicleById(@PathVariable Long vehicleId) {
		return vehicleRepository.findById(vehicleId).orElseThrow(() -> new ResourceNotFoundException("vehicle", "vehicleId", vehicleId));
	}
}
