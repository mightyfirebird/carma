package de.mightyfirebird.carma.controller;

import de.mightyfirebird.carma.exception.ResourceNotFoundException;
import de.mightyfirebird.carma.model.User;
import de.mightyfirebird.carma.payload.UserIdentityAvailability;
import de.mightyfirebird.carma.payload.UserProfile;
import de.mightyfirebird.carma.payload.UserSummary;
import de.mightyfirebird.carma.repository.UserRepository;
import de.mightyfirebird.carma.security.CurrentUser;
import de.mightyfirebird.carma.security.UserPrincipal;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@Log4j2
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping("/user/me")
	@PreAuthorize("hasRole('USER')")
	public UserSummary getCurrentUser(@CurrentUser final UserPrincipal currentUser) {
		final String joinedName = String.format("%s, %s", currentUser.getLastname(), currentUser.getFirstname());
		return new UserSummary(currentUser.getId(), currentUser.getUsername(), joinedName);
	}

	@GetMapping("/user/checkUsernameAvailability")
	public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
		Boolean isAvailable = !userRepository.existsByUsername(username);
		return new UserIdentityAvailability(isAvailable);
	}

	@GetMapping("/user/checkEmailAvailability")
	public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
		Boolean isAvailable = !userRepository.existsByEmail(email);
		return new UserIdentityAvailability(isAvailable);
	}

	@GetMapping("/users/{username}")
	public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
		final User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
		return new UserProfile(user.getId(), user.getFirstname(), user.getLastname(), user.getUsername(), user.getCreatedAt());
	}
}
