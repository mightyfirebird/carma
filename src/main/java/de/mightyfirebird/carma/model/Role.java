package de.mightyfirebird.carma.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

/**
 * Representation of user role.
 *
 * @author MightyFirebird
 * @version 6/30/2018
 */
@Entity
@Table(name = "roles")
@Getter
@Setter
@NoArgsConstructor
public class Role {
	/**
	 * Unique ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * Role name.
	 */
	@Enumerated(EnumType.STRING)
	@NaturalId
	@Column(length = 60)
	private RoleName name;

	/**
	 * Constructor.
	 *
	 * @param name Role name
	 */
	public Role(final RoleName name) {
		this.name = name;
	}
}
