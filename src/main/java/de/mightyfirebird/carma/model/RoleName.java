package de.mightyfirebird.carma.model;

/**
 * Enumeration of user roles.
 *
 * @author MightyFirebird
 * @version 6/30/2018
 */
public enum RoleName {
	ROLE_USER,
	ROLE_ADMIN
}
