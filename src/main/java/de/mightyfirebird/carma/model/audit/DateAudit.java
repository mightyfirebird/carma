package de.mightyfirebird.carma.model.audit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;

/**
 * Auditing for creation and modification date.
 *
 * @author MightyFirebird
 * @version 6/30/2018
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
		value = {"createdAt", "updatedAt"},
		allowGetters = true
)
@Getter
@Setter
public abstract class DateAudit implements Serializable {
	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -3000536340874001817L;

	/**
	 * Creation date.
	 */
	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Instant createdAt;

	/**
	 * Modification date.
	 */
	@LastModifiedDate
	@Column(nullable = false)
	private Instant updatedAt;
}
