package de.mightyfirebird.carma.model.audit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Auditing for creation and modification user.
 *
 * @author MightyFirebird
 * @version 7/4/2018
 */
@MappedSuperclass
@JsonIgnoreProperties(
        value = {"createdBy", "updatedBy"},
        allowGetters = true
)
@Getter
@Setter
public class UserDateAudit extends DateAudit {
    /**
     * Created By.
     */
    @CreatedBy
    @Column(updatable = false)
    private Long createdBy;

    /**
     * Updated By.
     */
    @LastModifiedBy
    private Long updatedBy;
}
