package de.mightyfirebird.carma.model;

import de.mightyfirebird.carma.model.audit.DateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users", uniqueConstraints = {
		@UniqueConstraint(columnNames = {
				"username"
		}),
		@UniqueConstraint(columnNames = {
				"email"
		})
})
@Getter
@Setter
@NoArgsConstructor
public class User extends DateAudit {
	/**
	 * Unique ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * First name.
	 */
	@NotBlank
	@Size(max = 50)
	private String firstname;

	/**
	 * Last name.
	 */
	@NotBlank
	@Size(max = 50)
	private String lastname;

	/**
	 * Username.
	 */
	@NotBlank
	@Size(max = 50)
	private String username;

	/**
	 * E-Mail-Address.
	 */
	@NaturalId
	@NotBlank
	@Size(max = 255)
	@Email
	private String email;

	/**
	 * Password.
	 */
	@NotBlank
	@Size(max = 100)
	private String password;

	/**
	 * Set of Roles.
	 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "role_id"))
	private transient Set<Role> roles = new HashSet<>();

	/**
	 * Constructor.
	 *
	 * @param firstname First name
	 * @param lastname  Last name
	 * @param username  Username
	 * @param email     E-Mail-Address
	 * @param password  Password
	 */
	public User(@NotBlank @Size(max = 50) String firstname, @NotBlank @Size(max = 50) String lastname,
	            @NotBlank @Size(max = 50) String username, @NotBlank @Size(max = 255) @Email String email,
	            @NotBlank @Size(max = 100) String password) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.email = email;
		this.password = password;
	}
}
