package de.mightyfirebird.carma.model;

import de.mightyfirebird.carma.model.audit.UserDateAudit;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "vehicles")
@Getter
@Setter
public class Vehicle extends UserDateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 50)
    private String make;

    @NotBlank
    @Size(max = 50)
    private String model;

    @NotBlank
    @Size(max = 50)
    private String subModel;

    @NotNull
    @Max(value = 9999)
    private int year;

    @NotBlank
    @Size(max = 50)
    private String engine;

    @NotBlank
    @Size(max = 500)
    private String note;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
