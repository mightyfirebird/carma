package de.mightyfirebird.carma.repository;

import de.mightyfirebird.carma.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * User-Repository.
 *
 * @author MightyFirebird
 * @version 6/30/2018
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	/**
	 * Finds User by E-Mail-Address.
	 *
	 * @param email E-Nail-Address
	 * @return {@link User}
	 */
	Optional<User> findByEmail(final String email);

	/**
	 * Finds User by username.
	 *
	 * @param username Username
	 * @return {@link User}
	 */
	Optional<User> findByUsername(final String username);

	/**
	 * Finds User by Username or E-Mail-Address.
	 *
	 * @param username Username
	 * @param email    E-Mail-Address
	 * @return {@link User}
	 */
	Optional<User> findByUsernameOrEmail(final String username, final String email);

	/**
	 * Finds Users based on List of User-IDs.
	 *
	 * @param userIds List of User-IDs
	 * @return List of {@link User}'s
	 */
	List<User> findByIdIn(final List<Long> userIds);

	/**
	 * Checks, whether a username already exists.
	 *
	 * @param username Username to check
	 * @return true, if username already exists, false otherwise
	 */
	Boolean existsByUsername(final String username);

	/**
	 * Checks, whether an E-Mail-Address already exists.
	 *
	 * @param email E-Mail-Address to check
	 * @return true, if email already exists, false otherwise
	 */
	Boolean existsByEmail(final String email);
}
