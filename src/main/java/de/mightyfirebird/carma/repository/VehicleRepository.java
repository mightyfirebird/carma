package de.mightyfirebird.carma.repository;

import de.mightyfirebird.carma.model.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    @Override
    Optional<Vehicle> findById(final Long vehicleId);

    List<Vehicle> findByUser(final Long userId);
}
