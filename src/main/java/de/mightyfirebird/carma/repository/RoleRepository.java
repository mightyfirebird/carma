package de.mightyfirebird.carma.repository;

import de.mightyfirebird.carma.model.Role;
import de.mightyfirebird.carma.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Role-Repository.
 *
 * @author MightyFirebird
 * @version 6/30/2018
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	/**
	 * Finds Role by RoleName.
	 *
	 * @param roleName {@link RoleName}
	 * @return {@link Role}
	 */
	Optional<Role> findByName(final RoleName roleName);
}
