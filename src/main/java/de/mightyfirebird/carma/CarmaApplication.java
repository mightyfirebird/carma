package de.mightyfirebird.carma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * CARMA - Car Maintenance Application
 *
 * @author MightyFirebird
 * @version 6/30/2018
 */
@SpringBootApplication
@EntityScan(basePackageClasses = {
		CarmaApplication.class,
		Jsr310JpaConverters.class
})
public class CarmaApplication {

	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	public static void main(String[] args) {
		SpringApplication.run(CarmaApplication.class, args);
	}


}
